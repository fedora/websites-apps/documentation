# Fedora Websites docs

Documentation for maintaining and adding new Fedora websites.
https://docs.fedoraproject.org/en-US/websites/

_NOTE_:
Fedora Badges documentation is no longer built from this repository.
Instead, you can find those docs at [fedora/websites-apps/fedora-badges/docs](https://gitlab.com/fedora/websites-apps/fedora-badges/docs).

## Local preview

This repo includes a script to build and preview the contents of this repository.

**NOTE**: Please note that if you reference pages from other repositories, such links will be broken in this local preview as it only builds this repository.
If you want to rebuild the whole Fedora Docs site, please see [the Fedora Docs build repository](https://gitlab.com/fedora/docs/docs-website/docs-fp-o) for instructions.

To build and preview the site, run:

```
$ ./docsbuilder.sh -p
```

The result will be available at http://localhost:8080

To stop the preview:

```
$ ./docsbuilder.sh -k

```

# License

SPDX-License-Identifier: CC-BY-SA-4.0
