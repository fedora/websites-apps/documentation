include::ROOT:partial$attributes.adoc[]

= Resources

The Websites & Apps Team is made up of people who collaborate on all levels of design, operations and technology. They value perspectives, opinions and experiences from a various points of views. Please check out these resources for your own local setup.

We are continually working to expand our established processes, instructional materials and documentation.

== Documentation
* link:++https://fedoraproject.org/w/uploads/1/19/Websitesandappsrevamp_logicmodel.png++[Fedora Websites Revamp Logic Model]
* link:++https://developer.fedoraproject.org/tech/languages/python/python-installation.html++[Python - Fedora Developer]
* link:++https://developer.fedoraproject.org/tech/languages/python/flask-installation.html++[Flask - Fedora Developer]
* For those interested in PHP and working with the link:++https://communityblog.fedoraproject.org++[Fedora Community Blog]
* link:++https://fedoramagazine.org/howto-install-wordpress-fedora/++[Wordpress Local Setup Instructions]

== Code Repositories
* link:++https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0[Fedora Websites]
* link:++https://pagure.io/fedora-websites++[Fedora Websites (Legacy)]
* link:++https://pagure.io/fedora-web/websites/++[Fedora Main Website (Legacy)]
* link:++https://github.com/fedora-infra/fedbadges++[Fedora Badges]
* link:++https://github.com/fedora-infra/noggin++[Fedora Account System(FAS)]
* link:++https://pagure.io/fedora-docs/docs-fp-o++[Fedora Docs]
