include::ROOT:partial$attributes.adoc[]

= Contact us

Have questions or want to know more?

== Connect with us

Get in touch with our team in the following places:

=== Asynchronous
** link:{COMMS-DISCOURSE}[Fedora Discussion]
** link:{COMMS-LISTSERV}[Mailing list]

=== Synchronous
** link:{COMMS-FEDORA-CHAT}[Fedora Chat]

== Membership

In order to become a member of the Fedora Website & Apps Team, the applicant must be registered at link:++https://accounts.fedoraproject.org++[Fedora Accounts] and set up an account with the link:++https://pagure.io++[Pagure] VCS forge.

By becoming a Fedora Contributor, you accept these terms.

* link:++https://fedoraproject.org/wiki/Legal:Fedora_Project_Contributor_Agreement++[Fedora Project Contributor Agrement (FPCA)]
* link:++https://docs.fedoraproject.org/en-US/project/code-of-conduct/++[Code of Conduct]
