include::engg:partial$attributes.adoc[]

= Per-project Tech Stack for Apps

This document elaborates upon the tech stacks that we use to develop and maintain the applications under our remit.

[[infographic]]
== Infographic
image:applications-A.png[]
image:applications-B.png[]

[[remit]]
== Our remit and tools

Following is a list of applications that we help develop and maintain.

'''

=== Badges
* Hosted at https://badges.fedoraproject.org/
* Maintained at
 - https://github.com/fedora-infra/fedbadges
 - https://github.com/fedora-infra/tahrir
 - https://github.com/fedora-infra/tahrir-api

==== Requires
- Pyramid
- Tahrir API
- Requests
- SQLAlchemy

'''

=== AskNot-NG
* Hosted at http://whatcanidoforfedora.org/
* Maintained at https://github.com/fedora-infra/asknot-ng

==== Requires
- JavaScript
- Mako
- PyYAML

'''

=== Elections
* Hosted at https://elections.fedoraproject.org/
* Maintained at https://github.com/fedora-infra/elections

==== Requires
- Flask
- Fedora Messaging
- Python Fedora
- FASJSON

'''

=== Nuancier
* Hosted at https://apps.fedoraproject.org/nuancier/
* Maintained at https://github.com/fedora-infra/nuancier

==== Requires
- Flask
- JavaScript
- SQLAlchemy
- Python Fedora

'''

=== Fedocal
* Hosted at https://calendar.fedoraproject.org/
* Maintained at https://github.com/fedora-infra/fedocal

==== Requires
- Flask
- Fedora Messaging
- SQLAlchemy
- Python Fedora

'''

=== Mote
* Hosted at https://meetbot.fedoraproject.org/
* Maintained at https://github.com/fedora-infra/mote

==== Requires
- Flask
- Fedora Messaging
- Bootstrap 5

'''

=== Commblog
* Hosted at https://communityblog.fedoraproject.org/

==== Requires
- Wordpress
- PHP

'''

=== Easyfix
* Hosted at https://fedoraproject.org/easyfix/
* Maintained at https://pagure.io/fedora-gather-easyfix

==== Requires
- Jinja
- Urllib3
- Bugzilla
- MWClient

'''

=== Magazine
* Hosted at https://fedoramagazine.org/

==== Requires
- Wordpress
- PHP

'''

=== Ambassador
* Hosted at https://fedoracommunity.org/
* Maintained at https://pagure.io/fedora-websites/blob/main/f/fedoracommunity.org

==== Requires
- TBD

'''