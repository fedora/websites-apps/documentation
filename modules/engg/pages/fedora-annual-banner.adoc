include::engg:partial$attributes.adoc[]

= How to add the Fedora Annual Survey Banner

== Add into start.fedoraproject.org

* https://pagure.io/fedora-websites[fedora-websites] repo, https://start.fedoraproject.org[*Start Fedora Project*]:
* data/templates/docs-header.html
* add a div with the link information
* `<div class="col-xs-12 col-sm-3 bluebox border">`
* NOTE: use col-sm-3 instead of 4 to fit 4 elements
* this will make it consistent with other on the page
* NOTE: for future modifications, check how the text renders when the browser window is smaller or on mobile.
* could be partially fixed by clamping font sizes

== How to remove the Fedora Annual Survey Banner

=== Removing From start.fedoraproject.org

* In https://pagure.io/fedora-websites[fedora-websites] repo, go to https://start.fedoraproject.org[*Start Fedora Project*]:
* edit data/templates/docs-header.html
* remove div and containing elements for the survey
* make sure to change the class to `<div class="col-xs-12 col-sm-4 bluebox border">`

=== Second method

NOTE: MIGHT NOT WORK, CURRENTLY TESTING THIS WITH NEST BANNER

* add a image to the data/content/index.html within the grey container
* use the class `col-xs-12 top-margin` and set padding with `style="padding:10px 0"`
* make the image responsive by giving it `style="max-width:832px; width:100%`


