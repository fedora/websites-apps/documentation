include::engg:partial$attributes.adoc[]

= Meetings

The team members collaborate periodically over synchronous and asynchronous meetings.

[[video-conference]]
== Video conference meetings

The Websites & Apps Engineering Team meets weekly as long as there are at least four active participants available. Should that not be the case - updates, plans and blockers are conveyed asynchronously in the Fedora Chat channel by the members.

[[conference-details]]
== Details
* Location - link:{COMMS-VIDEO}[*{COMMS-VIDEO-NAME} room*]
* Day - *{MEETING-DAY}s*
* Time - *{MEETING-TIME}*

[[agenda]]
== Agenda
A video conference meeting is a fleeting affair and hence, it is important to use every moment of the meeting wisely to discuss important topics.

=== Flow of discussion
* [_5m_] Roll call / greetings / welcomes
* [_5m_] Any announcements to share with group?
* [_5m_] Follow-ups on past actions, if any
* [_10m_] Engineering updates
* [_10m_] Council Objective updates
    - [_3m_] Mindshare updates
    - [_3m_] Stakeholders updates
    - [_4m_] General discussion
* [_remaining_] Discussion on plans and blockers
* Pick a chair for next time!
* [_5m before end_] Wrap-up and open floor discussion

=== Example
The following is a Markdown-styled example agenda to extend from.
[source]
----
## FORMAT

### Chair
- ADD YOUR NAME(S) HERE

### Attendees
- ADD YOUR NAME(S) HERE

### Check up
- How are you doing?
- How's the weather there?

### Agenda
- ADD ITEMS HERE
----

== How to chair team meetings?
Anyone can be a chair for the Websites and Apps Engineering Team, and we encourage everyone to try chairing our weekly team meetings at least once to get an experience. The team is supportive of newcomers and experienced folks alike who are trying to chair a meeting for the first time so one needs not worry about anything going wrong midway.

The chair should

* Be one of the first ones to join the meeting as they would be the ones coordinating with the attendees and carrying the meeting forward
* Have the agenda document open on a side to read topics from, add names to the agenda list and make notes/summaries/action-items discussed on
* Make sure that all the points from the slated agenda are discussed during the meeting and that the action items, if any, are accounted for
* Ensure that the attendees feel comfortable enough and get a fair amount of time to put their points forward and that there points are considered
* Act as an arbitrator to settle disputes, if there are any, among the attendees and try best to reach the middle ground by avoiding conflicts.
* Keep track of "raised hands" in order to ensure that every attendee in the meeting get their opportunity to speak in the order of "hands raised"
* Let the team know well in advance if they are not able to make it to the meeting that they volunteered to chair for to help get a replacement
* Attempt to prevent diversions in the meeting, be them caused by either themselves or the attendees and try to maximally utilize the meeting time
* Motivate communication in an accessible manner - so folks who are unwilling to use camera and/or microphone should be encouraged to use the chat
* Conduct an in-meeting poll, discussion, opinions, debate in an unbiased and fair manner to all the attendees to avoid any possible conflicts
* Feel comfortable to pass on their turn to someone else mid-meeting, should something important come up or if they cannot attend anymore
* Avoid a survivorship bias by passing on the updates shared and progresses made on the meeting to the wider community using relevant channels
* Make notes on the developments made in a meeting, summarize discussions, plans and action items, or find an assignee from the attendees to do it
* Ensure that they are attending the meeting from a noise-free and disturbance-free environment and are not distracted by any other work or social media
* Use an accessible and simple vocabulary to convey their statements for maximum participation, and wait enough after each point for attendees to reflect

[[guidelines]]
== Participation guidelines
* Please focus on listening, and on appreciating what others are trying to say, not only on what you are hearing.
* Wherever possible, please refrain from multitasking on email or social media and strive to remain fully present and tuned in to what others are saying and feeling.
* Please assume best intent in everyone's comments and strive to keep a constructive tone in your own.
* Please use simple, accessible language. In particular, please avoid jargon and acronyms, so that all may fully participate.
* When you speak, please make just one point and then let others speak. We want everyone to have an equal chance to speak. In a group of "n" people, speak no more than "1/nth" of the time.
* Please speak for yourself when making comments, using "I". Please don't speak for the assembled group by speaking as "we".
* Help us be mindful of the schedule and stay on time; we anticipate many people will have much they want to say, but please support us in moving the dialog forward.

[[self-aware]]
== Please be a self-aware participant!
* Please indicate you want to speak by raising your hand on video or with the button in Jitsi Meet; if you are not able to use or raise your hand, feel free to speak up but please try not to interrupt others.
* Please stay muted when you are not speaking.
* Please be mindful of background noise and join the call from a quiet location. Construction sites, windy gardens and non-quarantined internet cafes do not qualify as quiet locations.
* All parents and caregivers shall enjoy an irrevocable "background/foreground kid and dependent noise" exception at all times, but muting etiquette still applies.
